Pod::Spec.new do |s|

  s.name         = "BeaconLib"
  s.version      = "1.0.7"
  s.summary      = "BeaconLib framework."
  s.homepage     = "https://gitlab.com/InsightAR/OasisSDKLibs.git"
  s.license      = "MIT"
  s.author             = { "zhenghongsheng" => "zhenghongsheng@ezxr.com" }

  # 支持平台
  s.platform     = :ios
  s.platform     = :ios, '11.0'
  s.ios.deployment_target = '11.0'

  # 路径地址
  s.source       = { :git => 'https://gitlab.com/InsightAR/OasisSDKLibs.git', :tag => s.version }
  s.source_files  = 'BeaconLib.framework/Headers/*.{h}'
  # s.vendored_libraries = 'BeaconLib.framework/**/*'
  s.vendored_libraries = 'BeaconLib.framework'

end

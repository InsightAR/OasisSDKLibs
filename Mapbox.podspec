Pod::Spec.new do |s|

  s.name         = "Mapbox"
  s.version      = "1.0.7"
  s.summary      = "Mapbox SDK."
  s.homepage     = "https://gitlab.com/InsightAR/OasisSDKLibs.git"
  s.license      = "MIT"
  s.author             = { "zhenghongsheng" => "zhenghongsheng@ezxr.com" }

  # 支持平台
  s.platform     = :ios
  s.platform     = :ios, '11.0'
  s.ios.deployment_target = '11.0'

  # 路径地址
  s.source       = { :git => 'https://gitlab.com/InsightAR/OasisSDKLibs.git', :tag => s.version }
  s.source_files  = 'Mapbox.framework/Headers/*.{h}'
  s.vendored_frameworks = 'Mapbox.framework'

end
